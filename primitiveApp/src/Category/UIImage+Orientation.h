//
//  UIImage+Orientation.h
//  primitiveApp
//
//  Created by Nikita Kunevich on 2/15/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Orientation)

- (UIImage*)imageByNormalizingOrientation;

@end
