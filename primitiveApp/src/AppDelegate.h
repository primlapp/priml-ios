//
//  AppDelegate.h
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/8/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

