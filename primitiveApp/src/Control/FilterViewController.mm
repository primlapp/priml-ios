//
//  TGPhotoViewController.m
//  TGCameraViewController
//
//  Created by Bruno Tortato Furtado on 15/09/14.
//  Copyright (c) 2014 Tudo Gostoso Internet. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <AssetsLibrary/AssetsLibrary.h>

#import "FilterViewController.h"
#import "TGAssetsLibrary.h"
#import "TGCameraColor.h"
#import "UIImage+CameraFilters.h"
#import "TGTintedButton.h"

#import "Primitivizer.hpp"

@interface FilterViewController ()
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *shareBUtton;

@property (strong, nonatomic) IBOutlet UIImageView *photoView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;

@property (weak) id<TGCameraDelegate> delegate;
//@property (strong, nonatomic) UIView *detailFilterView;
@property (strong, nonatomic) UIImage *photo;
@property (strong, nonatomic) NSCache *cachePhoto;
@property (nonatomic) BOOL albumPhoto;

@property (weak, nonatomic) IBOutlet UIButton *circleButton;
@property (weak, nonatomic) IBOutlet UIButton *triangleButton;
@property (weak, nonatomic) IBOutlet UIButton *rectangleButton;
@property (weak, nonatomic) IBOutlet UIButton *polygonButton;
@property (weak, nonatomic) IBOutlet UIButton *comboButton;
@property (weak, nonatomic) IBOutlet UIButton *ellipseButton;

- (IBAction)backButtonClick:(UIButton *)sender;

- (IBAction)shareButtonClick:(UIButton *)sender;
- (IBAction)circleShapeSelected:(UIButton *)sender;
- (IBAction)triangleShapeSelected:(UIButton *)sender;

- (IBAction)squareShapeSelected:(UIButton *)sender;
- (IBAction)polygonShapeSelected:(UIButton *)sender;
- (IBAction)comboShapeSelected:(UIButton *)sender;
- (IBAction)ellipseShapeSelected:(UIButton *)sender;


//- (void)addDetailViewToButton:(UIButton *)button;

@end

#import "CGUtils.hpp"

@implementation FilterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (CGRectGetHeight([[UIScreen mainScreen] bounds]) <= 480) {
        _topViewHeight.constant = 0;
    }
    
    _photoView.clipsToBounds = YES;

    _photoView.image = _photo;
    
    primitive::PrimitivizerPtr primitivizer = primitive::Primitivizer::getInstance();
    primitivizer->Init(_photo.CGImage, 256);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark -
#pragma mark - Controller actions

- (IBAction)backButtonClick:(UIButton *)sender;
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)shareButtonClick:(UIButton *)sender;
{

}

- (void)unselectShapeButtons {
    [_circleButton setSelected:NO];
    [_triangleButton setSelected:NO];
    [_rectangleButton setSelected:NO];
    [_polygonButton setSelected:NO];
    [_comboButton setSelected:NO];
    [_ellipseButton setSelected:NO];
}

- (void)onAnyShapeSelected:(UIButton*)button {
    [self unselectShapeButtons];
    [button setSelected:YES];
}

- (IBAction)circleShapeSelected:(UIButton *)sender {
    [self onAnyShapeSelected:sender];
    [self applyShape:primitive::ShapeType::Circle];
}

- (IBAction)triangleShapeSelected:(id)sender {
    [self onAnyShapeSelected:sender];

    [self applyShape:primitive::ShapeType::Triangle];

}

- (IBAction)squareShapeSelected:(UIButton *)sender {
    [self onAnyShapeSelected:sender];

    [self applyShape:primitive::ShapeType::Rectangle];

}

- (IBAction)polygonShapeSelected:(id)sender {
    [self onAnyShapeSelected:sender];


    [self applyShape:primitive::ShapeType::Polygon];

}

- (IBAction)comboShapeSelected:(UIButton *)sender {
    [self onAnyShapeSelected:sender];

    [self applyShape:primitive::ShapeType::Combo];

}

- (IBAction)ellipseShapeSelected:(UIButton *)sender {
    [self onAnyShapeSelected:sender];

    [self applyShape:primitive::ShapeType::Ellipse];

}

- (void)applyShape:(primitive::ShapeType)shapeType {
    //        FUNCTON_START
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        auto primitivizer = primitive::Primitivizer::getInstance();
        primitivizer->processImage(shapeType, 1500);
        //        FUNCTION_STOP
        CGImageRef imageRef = primitivizer->getCurrentImage();
        
        UIImage* outImage = [UIImage imageWithCGImage:imageRef];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.photoView.image = outImage;
        });
        
        CGImageRelease(imageRef);
    });
    
}

#pragma mark -
#pragma mark - Private methods

//- (void)addDetailViewToButton:(UIButton *)button
//{
//    [_detailFilterView removeFromSuperview];
//    
//    CGFloat height = 2.5;
//    
//    CGRect frame = button.frame;
//    frame.size.height = height;
//    frame.origin.x = 0;
//    frame.origin.y = CGRectGetMaxY(button.frame) - height;
//    
//    _detailFilterView = [[UIView alloc] initWithFrame:frame];
//    _detailFilterView.backgroundColor = [TGCameraColor tintColor];
//    _detailFilterView.userInteractionEnabled = NO;
//    
//    [button addSubview:_detailFilterView];
//}

@end
