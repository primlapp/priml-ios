//
//  ViewController.m
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/8/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#import "MainViewController.h"
#import "Primitivizer.hpp"

#import <stdio.h>

#define FUNCTON_START   NSDate *startTime = [NSDate date];

#define FUNCTION_STOP   NSTimeInterval endTime = -[startTime timeIntervalSinceNow]; //printf("%s Time: %f",__PRETTY_FUNCTION__, -endTime);


@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation MainViewController

- (void)renderPrimitiveImage {
    NSArray* myImages = [[NSBundle mainBundle] pathsForResourcesOfType:@"jpg"
                                                           inDirectory:nil];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    UIImage *inputImage = [UIImage imageWithContentsOfFile:myImages.firstObject];
    CGImageRef imageRef = inputImage.CGImage;
    
    primitive::PrimitivizerPtr primitivizer = primitive::Primitivizer::getInstance();
    primitivizer->Init(imageRef, 256);
    
    double time[10];
    
    for (int i = 0; i < 10; i++) {
        FUNCTON_START
        primitivizer->processImage(primitive::ShapeType::Triangle, 150);
        FUNCTION_STOP
        imageRef = primitivizer->getCurrentImage();
        time[i] = endTime;
    }
    double sum;
    for (int i =0 ; i < 10; i++) {
        sum += time[i];
    }
    printf("avg = %lf\n", sum);

    UIImage* outImage = [UIImage imageWithCGImage:imageRef];
//    UIImage* blended = blendImages(inputImage, outImage);
    
    self.imageView.image = outImage;//blended;
//    self.imageView.transform = CGAffineTransformMakeRotation(M_PI);
    CGImageRelease(imageRef);
 
}


// to check difference in images
UIImage* blendImages(UIImage* firstImage, UIImage* secondImage)
{
    UIGraphicsBeginImageContext(firstImage.size); // Assumes the first image is the same size as the second image.
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0, firstImage.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetAlpha(context, 0.4f);
    CGContextDrawImage(context, CGRectMake(0, 0, [firstImage size].width, [firstImage size].height), firstImage.CGImage);
    CGContextSetAlpha(context, 0.8f);
    CGContextDrawImage(context, CGRectMake(0, 0, [firstImage size].width, [firstImage size].height), secondImage.CGImage);
    
    UIImage *coloredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return coloredImage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self renderPrimitiveImage];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark -
#pragma mark - TGCameraDelegate required

- (void)cameraDidCancel
{
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidTakePhoto:(UIImage *)image
{
//    _photoView.image = image;
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidSelectAlbumPhoto:(UIImage *)image
{
//    _photoView.image = image;
//    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - TGCameraDelegate optional

- (void)cameraWillTakePhoto
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)cameraDidSavePhotoAtPath:(NSURL *)assetURL
{
    NSLog(@"%s album path: %@", __PRETTY_FUNCTION__, assetURL);
}

- (void)cameraDidSavePhotoWithError:(NSError *)error
{
    NSLog(@"%s error: %@", __PRETTY_FUNCTION__, error);
}



@end
