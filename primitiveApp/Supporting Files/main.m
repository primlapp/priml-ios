//
//  main.m
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/8/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
